const { describe } = require('mocha');
const { add, substract, multiply, divide } = require('../src/calculator');
var chai = require('chai')
, assert = chai.assert
, expect = chai.expect
, should = chai.should();

describe("calculator", () => {
    describe("when substracting", () => {
        describe("10 from 15", () => {
            it("should return 5", () => {
                assert.equal(substract(15,10), 5);    
            })
        })
    })
    describe("when adding", () => {
        describe("10 to 50", () => {
            it("should return 60", () => {
                assert.equal(add(10,50), 60);    
            })
        })
    })
    describe("when multiplying", () => {
        describe("10 with 4", () => {
            it("should return 40", () => {
                assert.equal(multiply(10,4), 40);    
            })
        })
    })
    describe("when dividing", () => {
        describe("50 to 10", () => {
            it("should return 5", () => {
                assert.equal(divide(50,10),5);
            })
        })
    })
});



